# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the ibancheck.s60w79 package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: ibancheck.s60w79\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-11-23 13:18+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Main.qml:41 ibancheck.desktop.in.h:1
msgid "Iban Checker"
msgstr ""

#: ../qml/Main.qml:78
msgid "BIC (Bank Identifier Code)"
msgstr ""

#: ../qml/Main.qml:83
msgid "Name of payment recipient"
msgstr ""

#: ../qml/Main.qml:95
msgid "DE"
msgstr ""

#: ../qml/Main.qml:100
msgid "27"
msgstr ""

#: ../qml/Main.qml:105
msgid "701500001005836042"
msgstr ""

#: ../qml/Main.qml:112
msgid "IBAN is not valid."
msgstr ""

#: ../qml/Main.qml:121
msgid "Amount e.g. EUR301"
msgstr ""

#: ../qml/Main.qml:125
msgid "Use reference instead of description"
msgstr ""

#: ../qml/Main.qml:136
msgid "Reference number"
msgstr ""

#: ../qml/Main.qml:142
msgid "Description of usage"
msgstr ""

#: ../qml/Main.qml:147
msgid "Message to user"
msgstr ""

#: ../qml/Main.qml:176
msgid "Version"
msgstr ""

#: ../qml/Main.qml:191
msgid "Identification"
msgstr ""

#: ../qml/Main.qml:206
msgid "Usage (in 4 digits)"
msgstr ""

#: ../qml/Main.qml:218
msgid "Reset input"
msgstr ""

#: ../qml/Main.qml:238
msgid "View raw content"
msgstr ""

#: ../qml/Main.qml:243
msgid "View Code"
msgstr ""

#: ../qml/Main.qml:264
msgid "Swipe for Girocode"
msgstr ""

#: ../qml/Main.qml:274
msgid "Girocode"
msgstr ""
