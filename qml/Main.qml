/*
 * Copyright (C) 2022  S60W79
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ibancheck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "qqr.js" as QRCodeBackend
import "checks.js" as Checker

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'ibancheck.s60w79'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)
    property string qcode : "https://www.wikipedia.org"
    property bool ibanReCor : true
    property string preIban : ""
    Page {
        anchors.fill: parent
        
        header: PageHeader {
            id: header
            title: i18n.tr('Iban Checker')
        }
        
        Timer {
            id: timer
            interval: 2000; repeat: true
            running: true
            triggeredOnStart: true
            onTriggered: {
                Checker.ibanCh(line71.text, line72.text, line73.text, function (success){
                    //
                    if(success)ibanReCor=true;
                    if(!success)ibanReCor=false;
                });
                
            }
            }
            Flickable{
                id:liste
                // anchors{
                //     top:header.bottom
                //     left:parent.left
                //     right:parent.right
                //     bottom:parent.bottom
                // }
                clip:true
                flickableDirection: Flickable.VerticalFlick
                boundsBehavior:Flickable.DragOverBounds
                Column{
                    id:col
                    spacing:units.dp(5)
                    anchors.fill:parent
                    width:parent.width
                    height:parent.height
                    TextField {
                    id:line5
                    width:parent.width
                    placeholderText: i18n.tr("BIC (Bank Identifier Code)")
                    }
                    TextField {
                    id:line6
                    width:parent.width
                    placeholderText: i18n.tr("Name of payment recipient")
                    }
                    Label{
                        text:"\nIBAN (int. bank account number)"
                        font.bold:true
                    }
                    
                    Row{
                        width:parent.width
                        TextField {
                        id:line71
                        width:parent.width*0.15
                        placeholderText: i18n.tr("DE")
                        }
                        TextField {
                        id:line72
                        width:parent.width*0.15
                        placeholderText: i18n.tr("27")
                        }
                        TextField {
                        id:line73
                        width:parent.width*0.7
                        placeholderText: i18n.tr("701500001005836042")
                        }
                    }
                    Label{
                        visible:line71.text+line72.text+line73.text != "" && !root.ibanReCor
                        id:validHint
                        color:UbuntuColors.red
                        text:i18n.tr("IBAN is not valid.")
                    }
                    Label{
                        text:"(Example IBAN belongs to Ubports)\n"
                        font.italic:true
                    }
                    TextField {
                    id:line8
                    width:parent.width
                    placeholderText: i18n.tr("Amount e.g. EUR301")
                    }
                    Row{
                        Label{
                            text:i18n.tr("Use reference instead of description")
                        }
                        Switch {
                            id: reference
                            checked:false;
                        }
                    }
                    TextField {
                    id:line10
                    width:parent.width
                    visible:reference.checked
                    placeholderText: i18n.tr("Reference number")
                    }
                    TextField {
                    id:line11
                    width:parent.width
                    visible:!reference.checked
                    placeholderText: i18n.tr("Description of usage")
                    }
                    TextField {
                    id:line12
                    width:parent.width
                    placeholderText: i18n.tr("Message to user")
                    }
                    Button{
                        id:hider
                        width:parent.width
                        text:"Show advanced"
                        iconName:"view-list-symbolic"
                        onClicked:{
                            //
                            if(ad1.visible){
                                ad1.visible=false;
                                ad2.visible=false;
                                ad3.visible=false;
                                hider.text="Show advanced"
                            }else{
                                ad1.visible=true;
                                ad2.visible=true;
                                ad3.visible=true;
                                hider.text="Hide advanced"
                            }
                    
                            }
                        }
                    Row{
                        id:ad1
                        visible:false
                        width:parent.width
                        //
                        Label{
                            text:i18n.tr("Version")
                            width:parent.width/2
                        }
                        TextField {
                            id:line2
                            width:parent.width/2
                            text:"002"
                        }
                    }
                    Row{
                        visible:false
                        id:ad2
                        width:parent.width
                        //
                        Label{
                            text:i18n.tr("Identification")
                            width:parent.width/2
                        }
                        TextField {
                            id:line4
                            width:parent.width/2
                            text:"SCT"
                        }
                    }
                    Row{
                        visible:false
                        id:ad3
                        width:parent.width
                        //
                        Label{
                            text:i18n.tr("Usage (in 4 digits)")
                            width:parent.width/2
                        }
                        TextField {
                            id:line9
                            width:parent.width/2
                        }
                    }
                    Row{
                        width:parent.width
                        Button{
                            width:parent.width/3
                            text:i18n.tr("Reset input")
                            iconName:"delete"
                            onClicked:{
                                line2.text="002"
                                line4.text="SCT"
                                line5.text=""
                                line6.text=""
                                line71.text=""
                                line72.text=""
                                line73.text=""
                                line8.text=""
                                line9.text=""
                                line10.text=""
                                line11.text=""
                                line12.text=""
                            }
                            color: UbuntuColors.red
                        }
                        Button{
                            width:parent.width/3
                            text:i18n.tr("View raw content")
                            iconName:"empty-symbolic"
                        }
                        Button{
                            width:parent.width/3
                            text:i18n.tr("View Code")
                            iconName:"view-on"
                            color: UbuntuColors.green
                            onClicked:bottomEdge.commit();
                }
            }
                }
            }
        ScrollView {
            anchors{
                    top:header.bottom
                    left:parent.left
                    right:parent.right
                    bottom:parent.bottom
                }
            width: parent.width
            height: parent.height
            contentItem:liste
        }
        BottomEdge {
            id: bottomEdge
            hint.text: i18n.tr("Swipe for Girocode")
            height:parent.height
            hint.status:BottomEdgeHint.Active
            onCommitCompleted:root.qcode="BCD\n"+line2.text+"\n"+"1"+"\n"+line4.text+"\n"+line5.text+"\n"+line6.text+"\n"+line71.text+line72.text+line73.text+"\n"+line8.text+"\n"+line9.text+"\n"+line10.text+"\n"+line11.text+"\n"+line12.text
            // override bottom edge sections to switch to real content
            contentComponent:Page{
                width: bottomEdge.width
                height: bottomEdge.height
                header: PageHeader {
                id: qrheader
                title: i18n.tr('Girocode')
                }
                Flickable{
                    anchors{
                        top:qrheader.bottom
                        left:parent.left
                        right:parent.right
                        bottom:parent.bottom
                    }
                QRCode {
                    id:codec
                    width : Math.min(parent.width, parent.height)
                    height : Math.min(parent.width, parent.height)
                    value:root.qcode
        
                }
                }
                }
        }
        
    }
}
