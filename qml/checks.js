function ibanCh(country, check, number, callback){
            //
                            //Iban Checks
            
                if(number.length > 10){
                    //for Relevo...
                    //15 is minimal size (=norway)
                    if(preIban != country+check+number){
                        //only check if text changed to save energy
                        console.log("checking iban...");
                        preIban = country+check+number
                        function bigMod(divident, divisor) {
                            //calculating modolu for the large number to check
                            var partLength = 14;

                            while (divident.length > partLength) {
                                var part = divident.substring(0, partLength);
                                divident = (part % divisor) +  divident.substring(partLength);          
                            }

                            return divident % divisor;
                        }
                        var check = check
                        var cnum = (country[0].charCodeAt(0)-55).toString()+(country[1].charCodeAt(0)-55).toString()
                        var val = bigMod((number+cnum.toString()+check.toString()), 97)
                        if(val == 1){
                            callback(true);
                        }else{
                            callback(false);
                        }
                    }
                }else{
                    callback(false);
                }
        }
